<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('optimization_props', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('threshold');
            $table->string('source_event');
            $table->string('measured_event');
            $table->float('ratio_threshold');
        });

        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('optimization_props_id')->nullable();
            $table->foreign('optimization_props_id')->references('id')->on('optimization_props')->onDelete('cascade');
        });

        Schema::create('publishers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['install', 'purchase']);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('publisher_id');
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
        });

        Schema::create('publisher_blacklist', function (Blueprint $table) {
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('publisher_id');
            $table->primary(['campaign_id', 'publisher_id']);
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
        Schema::dropIfExists('publisher_blacklist');
        Schema::dropIfExists('campaigns');
        Schema::dropIfExists('optimization_props');
        Schema::dropIfExists('publishers');
    }
}
