LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
INSERT INTO `campaigns` VALUES (1,'Google',1);
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'install','2018-12-03 19:56:47',1,1),(2,'install','2018-12-03 17:57:40',1,1),(3,'purchase','2018-12-03 19:57:40',1,1);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2018_12_03_082244_initial',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `optimization_props` WRITE;
/*!40000 ALTER TABLE `optimization_props` DISABLE KEYS */;
INSERT INTO `optimization_props` VALUES (1,3,'install','purchase',0.70);
/*!40000 ALTER TABLE `optimization_props` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `publisher_blacklist` WRITE;
/*!40000 ALTER TABLE `publisher_blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `publisher_blacklist` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `publishers` WRITE;
/*!40000 ALTER TABLE `publishers` DISABLE KEYS */;
INSERT INTO `publishers` VALUES (1,'Hyundai');
/*!40000 ALTER TABLE `publishers` ENABLE KEYS */;
UNLOCK TABLES;