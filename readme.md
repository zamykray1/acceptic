1. Edit ```.evn``` file
2. ```composer install```
3. ```php artisan migrate```
4. ```mysql -u root -p pass dbname < database/migrations/data.sql```
5. Run ```php artisan optimization:job```