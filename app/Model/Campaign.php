<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Model\Campaign
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Campaign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Campaign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Campaign query()
 * @mixin \Eloquent
 */
class Campaign extends Model
{
    /** @var  int */
    private $id;

    /** @var  array */
    private $publisherBlacklist;

    /**
     * @return OptimizationProps|HasOne
     */
    public function getOptimizationProps() {
        return $this->belongsTo(OptimizationProps::class, 'id')->getResults();
    }
    public function getBlackList() {
        return $this->publisherBlacklist;
    }
    public function saveBlacklist(array $blacklist) {
        // dont implement
    }
    public function removeFromBlacklist(array $blacklist) {
        // dont implement
    }
    public function getId()
    {
        return $this->id;
    }
}