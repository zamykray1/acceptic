<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class OptimizationProps extends Model
{

    public function getCampaign()
    {
        return $this->hasOne(Campaign::class);
    }

    /**
     * @return int
     */
    public function getThreshold(): int
    {
        return $this->threshold;
    }

    /**
     * @param int $threshold
     */
    public function setThreshold(int $threshold): void
    {
        $this->threshold = $threshold;
    }

    /**
     * @return string
     */
    public function getSourceEvent(): string
    {
        return $this->source_event;
    }

    /**
     * @param string $sourceEvent
     */
    public function setSourceEvent(string $sourceEvent): void
    {
        $this->source_event = $sourceEvent;
    }

    /**
     * @return string
     */
    public function getMeasuredEvent(): string
    {
        return $this->measured_event;
    }

    /**
     * @param string $measuredEvent
     */
    public function setMeasuredEvent(string $measuredEvent): void
    {
        $this->measured_event = $measuredEvent;
    }

    /**
     * @return float
     */
    public function getRatioThreshold(): float
    {
        return $this->ratio_threshold;
    }

    /**
     * @param float $ratioThreshold
     */
    public function setRatioThreshold(float $ratioThreshold): void
    {
        $this->ratio_threshold = $ratioThreshold;
    }

}