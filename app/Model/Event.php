<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Event
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Event query()
 * @mixin \Eloquent
 */
class Event extends Model
{
    public function getType() {
        // for example "install"
        return $this->type;
    }
    public function getCampaignId() {
        return $this->campaign_id;
    }
    public function getPublisherId() {
        return $this->publisher_id;
    }

}