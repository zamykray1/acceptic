<?php

namespace App\Model;


class EventCountCollection extends AbstractCollection
{
    public function add($campaignId, $publisherId, $type, $cnt)
    {
        $this->data[$campaignId][$publisherId][$type] = $cnt;
    }

}