<?php

namespace App\Services;


use App\DataSource\CampaignDataSource;
use App\DataSource\EventsDataSource;

class OptimizationJob
{
    public function run():void {
        $campaignDS = new CampaignDataSource();
        $campaigns = $campaignDS->getCampaigns();
        $eventsDS = new EventsDataSource();

        $publisherNotifier = new PublisherNotifier();
        $publisherBlackList = [];
        foreach($eventsDS->getEventsSince("2 weeks ago") as $campaignId => $eventByPublisher) {
            $campaign = $campaigns[$campaignId];
            $props = $campaign->getOptimizationProps();

            $campaignBlacklist = [];
            $campaignWhitelist = [];
            foreach ($eventByPublisher as $publisherId => $eventCountByType) {
                $source = $eventCountByType[$props->getSourceEvent()] ?? 0;
                $measured = $eventCountByType[$props->getMeasuredEvent()] ?? 0;
                if ($source === 0) {
                    continue;
                }

                $ts = $measured / $source;
                if ($source >= $props->getThreshold()) {
                    if ($ts < $props->getRatioThreshold()) {
                        $campaignBlacklist[] = $publisherId;
                        $publisherBlackList[$publisherId]['add'][] = $campaign;
                    } else {
                        $campaignWhitelist[] = $publisherId;
                        $publisherBlackList[$publisherId]['remove'][] = $campaign;
                    }
                }
            }

            $campaign->saveBlacklist($campaignBlacklist);
            $campaign->removeFromBlacklist($campaignWhitelist);
        }

        foreach ($publisherBlackList as $publisherId => $campaignByType) {
            $publisherNotifier->blacklist($publisherId, $campaignByType['add'] ?? [], $campaignByType['remove'] ?? []);
        }
    }
}