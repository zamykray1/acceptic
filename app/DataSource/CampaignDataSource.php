<?php

namespace App\DataSource;


use App\Model\Campaign;

class CampaignDataSource
{
    /**
     * @return Campaign[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCampaigns()
    {
        return Campaign::all()->keyBy('id');
    }
}