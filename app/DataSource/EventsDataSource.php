<?php

namespace App\DataSource;


use App\Model\EventCountCollection;
use Illuminate\Support\Facades\DB;

class EventsDataSource
{
    /**
     * @param string $period
     * @return EventCountCollection
     */
    public function getEventsSince(string $period): EventCountCollection
    {
        $from = (new \DateTime())->modify($period);

        $rawData = Db::select('Select count(*) as cnt, campaign_id, publisher_id, type from events
            where created_at >= ?
            group by campaign_id, publisher_id, type', [$from->format('Y-m-d H:i:s')]);

        $eventCountCollection = new EventCountCollection();
        foreach ($rawData as $row) {
            $eventCountCollection->add($row->campaign_id, $row->publisher_id, $row->type, $row->cnt);
        }

        return $eventCountCollection;
    }
}