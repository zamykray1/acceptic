<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OptimizationJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optimization:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $optimizationJob = new \App\Services\OptimizationJob();
        $optimizationJob->run();
    }
}
